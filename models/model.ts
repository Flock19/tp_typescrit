import Order from 'controllers/Order'
import storage from 'node-persist'
export default class model{

    public static findOrders = () => storage.getItem('orders')

    public static findId = (orders : any,id : any) => orders.find((order: any) => order.id === parseInt(id, 10))
    
    public static async findPayload(payload : any){
    const orders = await this.findOrders()
    const alreadyExist = orders.find((order: any) => order.id === payload.id)
    return alreadyExist
    }
    public static setOrders = async (orders : any) => await storage.setItem('orders', orders)

}
