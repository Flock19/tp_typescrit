import { Router, Request, Response } from 'express'
import index from '../controllers/indexController'

const router = Router()

router.get('/',  (req: Request, res: Response) => {
  index.Helloword(req,res);
})
router.get('/favicon.ico', (req: Request, res: Response) => res.status(204))

export default router