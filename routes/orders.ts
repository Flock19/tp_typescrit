import { Router, Request, Response } from 'express'
import ordersCo from '../controllers/ordersController'
import orders from '../controllers/ordersController'

const router = Router()

router.get('/', async (req: Request, res: Response) => { orders.jsonify(req,res)
})
router.get('/:id', async (req: Request, res: Response) => { orders.id(req,res) })

router.post('/', async (req: Request, res: Response) => {
  ordersCo.addOrder(req,res)
})

export default router