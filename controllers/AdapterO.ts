import Order from "./Order"
import { IPackage } from "types";
import {IDelivery} from "types";

export default class AdapterO extends Order{
    
    constructor(id : number,packages : [IPackage] ,delivery : IDelivery){
        super(id,packages,delivery);
        this.Anonymer();
    }
    public Anonymer(): void{
        
        this.delivery.contact =  {
            fullname: '*blank*',
            email: '*blank*',
            phone: '*blank*',
            address: '*blank*',
            postalCode: '*blank*',
            city: '*blank*',
          }
    }
}