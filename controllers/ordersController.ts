import storage from 'node-persist'
import model from '../models/model'
import AdapterO from '../controllers/AdapterO'
import Order from '../controllers/Order'
export default class ordersCo {

    public static jsonify = async  (req : any, res : any) => {

      const orders : Order[] = await model.findOrders()
      const anoOrders = orders.map(order => new AdapterO(order.id, order.packages, order.delivery))
      res.json(anoOrders)
    }

    public static id = async (req : any,res : any) => {
    const id = req.params.id
    const orders = await model.findOrders()
    const result = model.findId(orders,id)
    if (!result) {
      return res.sendStatus(404)
    }
    return res.json(result)
    }

    public static addOrder = async (req :any, res : any) => {
    
    const payload = req.body
    const orders = await model.findOrders()
    const alreadyExists = model.findPayload(payload)
  
    if (alreadyExists) {
      return res.sendStatus(409)
    }
  
    orders.push(payload)
  
    //await storage.setItem('orders', orders)
    model.setOrders(orders)
  
    res.sendStatus(201)
    }

}