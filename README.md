
                                       ****REPONSES AUX QUESTIONS****
#2 Le design pattern que j'ai choisi est le MVC parce qu'il apporte de la clarté à l'architecture du projet et permet de subdiviser le projet global en différents en fonction de leurs roles (Modèle, Vue et Controlleur)

#3 Le design pattern du SOLID adapté à cette question le "I" qui signifie littéralement " Interface segregation principe" qui consiste simple à organiser ou hierarchiser les interfaces et ensuite les implémenter

#4 Comme il s'agit de prendre des éléments précis d'un object qu'il faut adapter à un contexte d'utilisation particulier, en l'occurrence "anonymiser" les informations du contact, le deign pattern préconisé est l'adaptateur. Comme son nom l'indique, il adapte un object pour une utilisation à une utilisation qui n'etait initialement pas prévue.   

