import express from 'express'
import path from 'path'
import cookieParser from 'cookie-parser'
import logger from 'morgan'

import indexRouter from './routes/index'
import ordersRouter from './routes/orders'
import Storage from './Storage'

export default class App{

public instance: any 
public Storage : any

constructor(){

  this.instance = express()

  this._registerMiddlewares()
  this._registerRoutes()
  this.Storage = new Storage("orders")
}

private _registerMiddlewares(): void{
  this.instance.use(logger('dev'))
  this.instance.use(express.json())
  this.instance.use(express.urlencoded({ extended: false }))
  this.instance.use(cookieParser())
  this.instance.use(express.static(path.join(__dirname, 'public')))
}
private _registerRoutes(): void{
  this.instance.use('/', indexRouter)
 this.instance.use('/orders', ordersRouter)
}
}
